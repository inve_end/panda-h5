var url =  'ws://161.117.195.63:8887' ;  //这里的socket连接地址 最好是动态获取
var ws = '';
var ReconnerctTimer = 0 ;  
var ConnectFlag = true ;  //设置连接标识

import store from '../store/module/socket/socket'

export function  Reconnect(obj)
  {
    //连接校验
    if ( ws != '') return false;
    ws = new WebSocket(url);
    
    ws.onopen = (e) => {OnConnect(e);console.log('onopen=>%o'  , e);};
    ws.onclose = (e) => {OnClose(e,obj);console.log('onopen=>%o'  , e);};
    ws.onerror = (e) => {OnClose(e,obj);console.log('onopen=>%o'  , e);};
    ws.onmessage = (e) => {ServerMessage(e.data, obj); console.log('onmessage=>%o'  , e);}  
   
}

//关闭 
export function OnClose(e,obj){
    
    ConnectFlag = false;
    //启动断线重连计时器
    if (ReconnerctTimer == 0)
    {
        console.log('连接断开,%o秒后开始重连...', 3000);
        ReconnerctTimer = setInterval(()=>Reconnect(), 3000);
    }
}

//连接
export function OnConnect(e){
    if (ReconnerctTimer != 0)
    {
        console.log('清除重连计时器...');
        clearInterval(ReconnerctTimer);
        ReconnerctTimer = 0;
    }

    ConnectFlag = true;
}

//消息处理
export function ServerMessage(data, obj){ 
    let socketData = JSON.parse(data) ;
    if( socketData.commandCode == 2 ){
       obj.dispatch('set_socket_data', socketData);
       console.log(obj);
    }else if( socketData.commandCode == 1 ){
       obj.dispatch('set_bet_socket_data', socketData);
    }
}