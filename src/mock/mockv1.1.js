// 获取玩法详情接口
export const betinfo = [
  {
    betType: [
      {
        awayTeamName: "测试内容iul0",
        awayTeamOdds: "测试内容es38",
        drawOdds: "测试内容w9b3",
        homeTeamName: "测试内容cab5",
        homeTeamOdds: "测试内容m494",
        matchType: "测试内容2o1s"
      }
    ],
    code: 64326,
    matchInfo: {
      awayRedCard: 35501,
      awayTeamLicon: "测试内容pt5e",
      awayTeamName: "测试内容jfi2",
      homeLicon: "测试内容5111",
      homeRedCard: 50184,
      homeTeamName: "测试内容iqo4",
      matchDate: 88710,
      matchStatus: 44451,
      matchid: 38324,
      socre: "测试内容msik"
    },
    status: "测试内容xib4",
    tournament: "测试内容1o57"
  }
];

//获取体育类型接口
export const sport_type = {
  Sports: [
    {
      SportName: "测试内容uy86",
      Sportid: 62106,
      count: 68767,
      tatalPlay: 88643
    }
  ],
  code: 24471,
  status: "测试内容yd8x"
};

//获取联赛数据接口
export const tournamentlist = {
  code: 200,
  status: "测试内容6g17",
  tournamentList: [
    {
      id: 25541,
      level: 46170,
      name: "测试内容sd48",
      picUrl: "测试内容65do",
      totalMatchs: 25886
    },
    {
      id: 25541,
      level: 46170,
      name: "测试内容sd48",
      picUrl: "测试内容65do",
      totalMatchs: 25886
    },
    {
      id: 25541,
      level: 46170,
      name: "测试内容sd48",
      picUrl: "测试内容65do",
      totalMatchs: 25886
    }
  ]
};

//获取赛事数据接口

export const matchInfo = {
  code: 200,
  matchInfo: [
    {
      AwayRedCard: 61346,
      AwayTeam: "测试内容j4nq",
      AwayTeamId: 68186,
      EventDate: 13020,
      EventStatusId: 25143,
      GroundTypeId: 32734,
      HomeRedCard: 32417,
      HomeTeam: "测试内容8w4w",
      HomeTeamId: 57561,
      MarketLines: [
        {
          MarkelineStatusId: 86722,
          MarketLineId: 40663,
          MarketLineLevel: 17180,
          PeriodId: 62786,
          PeriodName: 52880
        }
      ],
      OddsList: [
        {
          OddsType: 21518,
          OddsValues: "测试内容ndt4"
        }
      ],
      OrderNumber: 87574,
      OutrightEventName: "测试内容xd15",
      WagerSelections: [
        {
          Handicap: "测试内容1t79",
          SelectionId: 80143,
          SelectionName: "测试内容fa9l",
          WagerSelectionId: 37375
        }
      ],
      matchid: 11543
    }
  ],
  status: "测试内容1cn9"
};
//菜单栏目初始化接口
export const menu_init = {
  ver: "1.1.0000",
  status: true,
  msg: "成功",
  code: 200,
  data: [
    {
      id: 4,
      name: "滚球",
      value: null,
      type: 1,
      sportId: null,
      parentId: null,
      children: [
        {
          id: 7,
          name: "全部",
          value: null,
          type: 1,
          sportId: null,
          parentId: 4,
          children: []
        },
        {
          id: 8,
          name: "足球",
          value: null,
          type: 1,
          sportId: 1,
          parentId: 4,
          children: []
        }
      ]
    },
    {
      id: 5,
      name: "即将开赛",
      value: null,
      type: 2,
      sportId: null,
      parentId: null,
      children: [
        {
          id: 9,
          name: "全部",
          value: null,
          type: 2,
          sportId: null,
          parentId: 5,
          children: []
        },
        {
          id: 10,
          name: "足球",
          value: null,
          type: 2,
          sportId: 1,
          parentId: 5,
          children: []
        }
      ]
    },
    {
      id: 6,
      name: "足球",
      value: null,
      type: 5,
      sportId: 1,
      parentId: null,
      children: [
        {
          id: 13,
          name: "滚球",
          value: null,
          type: 1,
          sportId: 1,
          parentId: 6,
          children: []
        },
        {
          id: 11,
          name: "今日赛事",
          value: null,
          type: 3,
          sportId: 1,
          parentId: 6,
          children: []
        },
        {
          id: 12,
          name: "早盘",
          value: null,
          type: 4,
          sportId: 1,
          parentId: 6,
          children: [
            {
              id: null,
              name: "全部",
              value: null,
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            },
            {
              id: null,
              name: "2019-09-06",
              value: "2019-09-06",
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            },
            {
              id: null,
              name: "2019-09-07",
              value: "2019-09-07",
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            },
            {
              id: null,
              name: "2019-09-08",
              value: "2019-09-08",
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            },
            {
              id: null,
              name: "2019-09-09",
              value: "2019-09-09",
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            },
            {
              id: null,
              name: "2019-09-10",
              value: "2019-09-10",
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            },
            {
              id: null,
              name: "2019-09-11",
              value: "2019-09-11",
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            },
            {
              id: null,
              name: "2019-09-12",
              value: "2019-09-12",
              type: 4,
              sportId: 1,
              parentId: null,
              children: null
            }
          ]
        }
      ]
    }
  ],
  serverTime: 1567746975237
};
//菜单栏目加载赛事统计接口
export const menu_count = {
  ver: "1.1.0000",
  status: true,
  msg: "成功",
  code: 200,
  data: [
    { lid: 8, count: 20 },
    { lid: 13, count: 20 },
    { lid: 10, count: 30 },
    { lid: 4, count: 20 },
    { lid: 5, count: 30 },
    { lid: 7, count: 20 },
    { lid: 9, count: 30 }
  ],
  serverTime: 1567747360110
};
//赛事详情页面接口
export const matchDetail_getMatchInfo = {
  code: 53337,
  data: {
    matchDetail: {
      awayTeamName: "测试内容lf07",
      canParlay: 28167,
      cornerKickScore: "测试内容2p7s",
      currentScore: "测试内容t98x",
      fullScore: "测试内容udr8",
      halfScore: "测试内容2pwn",
      homeTeamName: "测试内容75ft",
      id: 72241,
      liveOddsBetStatus: 50143,
      matchId: 40818,
      matchStatus: 66635,
      matchTime: 51845,
      neutralGround: 52023,
      overScore: "测试内容8nk9",
      preMatchBetStatus: 84558,
      redCard: "测试内容pedh",
      secondsMatchStart: 40683,
      tournamentName: "测试内容rx5n",
      yellowCard: "测试内容v52p"
    }
  },
  msg: "测试内容gwr9",
  oddsInfo: [
    {
      liveOdds: 66566,
      oddsFieldList: [
        {
          active: 66571,
          fieldOddsValue: 85070,
          footballOddsId: 33145,
          operateTock: 47201,
          orderOdds: 56611,
          showsValue: "测试内容22si"
        }
      ],
      oddsName: "测试内容sp27",
      oddsTypeId: 51152,
      oddsValue: "测试内容3r31",
      operateLock: 51252,
      startTime: 65714
    }
  ],
  status: false
};

//根据玩法查询盘口信息
export const matchDetail_getMatchOddsInfo = {
  code: 200,
  data: {
    active: 0,   //当前玩法是否被激活.	number	1激活；0未激活(锁盘)
    canceled: 0,   //	当前玩法是否被取消.	number	.1取消；0未取消
    i18Name: null,   //	多语言对应翻译		
    id: 9,   //	盘口id	number	
    liveOdds: 0,   //	是否为滚球盘口.	number	1 是； 0 否
    marketCategoryId: 0,   //	玩法id	number	
    marketCategoryName: null,   //	玩法名称	string	
    modifyTime: 0,   //	修改时间	number	
    modifyUser: 0,   //	修改人	number	
    nameCode: null,   //	盘口名称编码. 用于多语言	string	
    oddsFieldList: {
      active: 0,   //	是否被激活.	number	当前交易项是否被激活.1激活；0未激活(锁盘)
      endTime: 0,   //	结束时间	number	该交易项从该字段指定的时间停止接受下注
      fieldOddsValue: 1000,   //	交易项赔率。	string	单位：0.0001
      footballOddsId: 0,   //	盘口id	number	
      id: 0,   //	交易项ID	number	
      managerConfirmPrize: 0,   //	是否需要人工确认开奖	number	1 需要； 0 不需要
      name: 0,   //		string	
      nameCode: 0,   //	多语言编码	number	
      oddsFieldsTempletId: 0,   //	模板id	number	
      operateLock: 0,   //	是否锁盘	number	1 锁盘； 0 不锁盘。锁盘后，用户不可下注。
      orderOdds: 0,   //	用于排序，大于1，越小越靠前	number	
      standardMatchId: 0,   //	比赛id	number	
      standardTournamentId: 0,   //	联赛id	number	
      standardTournamentId: 0,   //	联赛id	number	
      startTime: 0,   //	开始时间	number	该交易项从该字段指定的时间开始接受下注。
      targetSide: '粉色',   //	主、客、和	string	主：T1,客：T2 和：NULL
      visible: 1,   //	是否可见	number	1：可见； 0：不可见
    },
    oddsName: '名称',   //	盘口名称	string	
    oddsValue: 0,   //	盘口显示值	string	
    operateLock: 0,   //	是否锁盘	number	1 锁盘； 0 不锁盘。锁盘后，用户不可下注。
    settled: 0,   //	是否结算	number	1 结算结束； 0 未结束
    standardMatchInfoId: 0,   //	比赛id	number	比赛id
    startTime: 0,   //	开始时间	number	.该盘口从该字段指定的时间开始接受下注。创建盘口数据时，
    title: 0,   //	盘口展示信息	array	title": [ "中国", "和局", "日本" ]
    visible: 1,   //	是否可见	number	1：可见； 0：不可见
  },
  msg: 0,   //返回信息	string	提示信息
  status: true
};