import {mapGetters } from "vuex";

export default {
  data() {
    return {
      head_left_title:'足球'
    }
  },
  computed: {
   ...mapGetters({
    top_title_arr:'get_top_title'
   })
  },
  watch: {
    top_title_arr(newValue, oldValue) {
      this.recalculate_head_left_title()
    }
  },
  methods: {
    recalculate_head_left_title() {
      let arr=this. top_title_arr
      let str='';
      arr[0] && (str+=`${arr[0]}`);
      arr[1] && (str+=` ${arr[1]}`);

      this.head_left_title =str

    }
  },

};
