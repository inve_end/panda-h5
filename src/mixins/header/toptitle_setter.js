import { mapActions } from "vuex";

export default {

  methods: {
    ...mapActions({
      set_top_title: "set_top_title"
    })
  }
};
