/**
 * 此文件  只是 控制 登录窗口的 显示和隐藏
 * 请勿 引入其他内容
 * 
 * 修改人: @jinnian
 * 注释时间: 2019-09-07 10:25:21
 */


import {mapGetters, mapActions } from 'vuex'
export default {

    methods: {
        ...mapActions([
            'set_to_show_login', //also supports payload `this.nameOfAction(amount)` 
        ])
    },
}

