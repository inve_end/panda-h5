export default {
    data(){
        return {
            use_mock_data:false,
            test_select_option:[
                { label: '占位数据0', value: 0,id:0 },
                { label: '占位数据1', value: 1,id:1 },
                { label: '占位数据2', value: 2,id:2 },
                { label: '占位数据3', value: 3,id:3 },
                { label: '占位数据4', value: 4,id:4 },
                { label: '占位数据5', value: 5,id:5 },
                { label: '占位数据6', value: 9,id:6 },

            ]
        }
    }
}