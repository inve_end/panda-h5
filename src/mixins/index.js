// common & constant
import baseconfigmixin from "src/mixins/module/baseconfigmixin.js";
// import constantmixin from "src/mixins/constant/index.js";
import commontoolmixin from "src/mixins/common/commontoolmixin.js";

//layout

// import rightinfo from "src/mixins/layout/rightinfo.js"

// module
import handlemixin from "src/mixins/module/handlemixin.js";

import alertmessagemixin from "src/mixins/module/alertmessagemixin.js";
// import alllanguage from "src/mixins/language/alllanguage.js";

// login
import toshowlogin from "src/mixins/login/toshowlogin.js";

// tpomenu

import tpomenumixin from "src/mixins/topmenu/tpomenu.js";

//test
import testmixin from "src/mixins/test/test.js";

export default [
  alertmessagemixin,
  baseconfigmixin,
  // ...constantmixin,
  commontoolmixin,
  tpomenumixin,
  handlemixin,
  toshowlogin,

  testmixin
];
