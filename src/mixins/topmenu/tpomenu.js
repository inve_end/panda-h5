
/**
 * 传递头部两级菜单导航栏目触发的 最终请求参照对象
 * 
       id	菜单ID	number	查询统计接口数据与之绑定
        name	菜单显示名称	string	
        parentId	父类菜单ID	number	
        sportId	菜单绑定的体育种类ID	number	查询赛事时需要传递到后台
        type	菜单类型	number	编码类型 1 滚球 2 即将开赛 3 今日赛事 4 早盘 5 赛种
        value	菜单附加属性值	string	不被后台主动维护的菜单如早盘节点下的菜单 查询早盘数据时携带的附加参数
 * 
 * 
 * 修改人: @jinnian
 * 注释时间: 2019-09-06 13:31:34
 */



import { mapGetters } from "vuex";
export default {
data(){
    return{
        
    }
},
computed: {
    ...mapGetters({
        active_topmenu:'get_topmenu',
        menu_count_data:'get_menu_count'
    })
},
created() {

},
methods: {
    compute_cur_menu_count(item) {
        let num = 0;
  
        for (let x = 0; x < this.menu_count_data.length; x++) {
          if (this.menu_count_data[x].lid == item.id) {
            num = this.menu_count_data[x].count;
            break;
          }
        }
        return num;
      },
    
},
}