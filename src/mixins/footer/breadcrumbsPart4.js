
/**
 *  底部  面包屑导航 的 操作状态的 第四 部分 
 */
import { mapActions } from "vuex";
export default {

    methods: {
        ...mapActions({
            set_breadcrumbsPart4:'set_breadcrumbsPart4'
        })
        
    },
}