/**
 *
 *
 */

export default {
  data() {
    return {
      show_goback: false,
      show_goback_routenames: ["odds_info"]
    };
  },
  created() {
    this.compute_show_goback();
  },
  watch: {
    $route(newValue, oldValue) {
      this.compute_show_goback();
    }
  },
  methods: {
    compute_show_goback() {
      console.log(
        "================================================================================="
      );
      if (this.show_goback_routenames.includes(this.$route.name)) {
        this.show_goback = true;
      } else {
        this.show_goback = false;
      }
    },
    handler_to_goback() {
      this.$router.go(-1);
    }
  }
};
