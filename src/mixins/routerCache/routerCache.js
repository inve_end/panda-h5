import { mapGetters, mapMutations } from 'vuex'

export default {
  methods: {
    ...mapMutations(['set_router_cache', 'set_use_router_cache']),
    //返回提交对象
    use_router_cache_need (params) {
      this.set_router_cache({name: this.$route.name, params})
      let name = this.router_cache.get(this.$route.name)
      if (this.use_router_cache && name) {
        // 关闭缓存
        this.set_use_router_cache(0);
        return name
      } else {
        return params
      }
    }
  },
  computed: {
    ...mapGetters({
      router_cache: 'get_router_cache',
      use_router_cache: 'get_use_router_cache'
    })
  },
}