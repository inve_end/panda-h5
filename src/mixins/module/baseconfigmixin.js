
export default {
  data() {
    return {
      file_server_address:"http://161.117.194.247:7788/",
      currentPage: 1,
      maxPageNum: 1,
      total: 0,
      // pageSize:50,
      pageSizeOptions: [10, 20, 50, 100],
      pagination: {
        sortBy: '',
        descending: false,
        page: 1,
        rowsPerPage: 10,
         rowsNumber: 0,
      },
      
      separator:'cell',//'horizontal','vertical','none','cell' 
      tabletittle:'',
      tabledata:[],
      tablecolumns:[
      ],
      tabledata_loading:false,
      tabledata_can_request:true,

      
    }
  },
}
