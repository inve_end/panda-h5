export default {
    methods: {
        format_date_by_manage(value) {
            let time = new Date(parseInt(value));
            let y = time.getFullYear();
            // let m = (time.getMonth() + 1 + "").padStart(2, 0);
            let m = time.getMonth() + 1 + "";
            // let d = (time.getDate() + "").padStart(2, 0);
            let d = time.getDate() + ""
            let h = (time.getHours() + "").padStart(2, 0);
            let mm = (time.getMinutes() + "").padStart(2, 0);
            let s = (time.getSeconds() + "").padStart(2, 0);
            let arr = ["日", "一", "二", "三", "四", "五", "六"];
            let i = time.getDay();
            let weekday = "周" + arr[i];
            return `${m}月${d}日 (${weekday})`;
        },
        format_day(value, separator) {
            if (!value) { return '' }
            let [y, m, d, h, mm, s] = this.format_date_base(value)
            separator = separator || '/'
            return `${y}${separator}${m}${separator}${d}`
        },
        format_month(value, separator) {
            if (!value) { return '' }
            // utc转成gmt+8
            let time = parseInt(value) + 8 * 60 * 60 * 1000
            let [y, m, d, h, mm, s] = this.format_date_base(time)
            separator = separator || '/'
            return `${m}${separator}${d} ${h}:${mm}:${s}`
        },
        format_date(value) {
            if (!value) { return '' }
            let [y, m, d, h, mm, s] = this.format_date_base(value)
            return `${y}-${m}-${d} ${h}:${mm}:${s}`;
        },
        format_date_base(value) {
            let time = new Date(parseInt(value));
            let y = time.getFullYear();
            let m = (time.getMonth() + 1 + "").padStart(2, 0);
            let d = (time.getDate() + "").padStart(2, 0);
            let h = (time.getHours() + "").padStart(2, 0);
            let mm = (time.getMinutes() + "").padStart(2, 0);
            let s = (time.getSeconds() + "").padStart(2, 0);
            return [y, m, d, h, mm, s]
        },
        format_week(value) {
            let arr = ["日", "一", "二", "三", "四", "五", "六"];
            let i = new Date().getDay();
            return "周" + a[i];
        },
        utc_to_gmt_8(value) {
            if (!value) { return '' }
            let time = parseInt(value) + 8 * 60 * 60 * 1000
            let [y, m, d, h, mm, s] = this.format_date_base(time)
            return `${m}/${d} ${h}:${mm}`;

        },
        utc_to_gmt_8_hm(value) {
            if (!value) { return '' }
            let time = parseInt(value) + 8 * 60 * 60 * 1000
            let [y, m, d, h, mm, s] = this.format_date_base(time)
            return `${h}:${mm}`;

        },
        utc_to_gmt_8_ms(value) {
            if (!value) { return '' }
            let time = parseInt(value) + 8 * 60 * 60 * 1000
            let [y, m, d, h, mm, s] = this.format_date_base(time)
            return `${mm}'${s}'`;
        },
        utc_to_label_show(value){
            if (!value) { return '' }
            let str=''
            let time = parseInt(value) + 8 * 60 * 60 * 1000
            let time_local=new Date().getTime();
            if(time>time_local){
                let cha_m= Math.floor((time-time_local) /(60*1000) )
                str=`${cha_m}分钟后开始`
            }else{
                str=`已开赛`
            }
            return str
        },
        gmt_to_label_show(value){
            if (!value) { return '' }
            let str=''
            let time = parseInt(value) ;
            let time_local=new Date().getTime();
            if(time>time_local){
                let cha_m= Math.floor((time-time_local) /(60*1000) )
                str=`${cha_m}分钟后开始`
            }else{
                str=`已开赛`
            }
            return str
        },
    },


}