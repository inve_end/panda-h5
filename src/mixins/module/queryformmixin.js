export default {
    data(){
        return{
            // queryform_form:{},
            // queryform_module:{},

        }
    },
    created() {
        this.init_queryform();
    },
    watch: {
        queryform_form:{
            handler:function(){
              console.log("queryform_form  改变了  ");
            console.log(this.queryform_form );
            // this.initPageSizeAndCurrentPage(this.pageSize, 1,1)   
            this.restart_init_tabledata()
            },
            deep: true 
        }
    },
    methods: {
        
        init_queryform() {
            if (this.queryform_form) {
                this.reset_queryform()
            }
        },
        tosearch_queryform() {
            this.init_tabledata();
        },
        reset_queryform() {
            this.queryform_form = this.deep_clone(this['queryform_module']);
        },

    },
}