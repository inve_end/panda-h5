import { mapGetters, mapActions } from "vuex";
export default {
  data() {
    return {};
  },
  computed: {
    ...mapGetters({
      get_pageSize: 'get_pageSize',
    })
  },
  created() {
    this.compute_maxPageNum();
  },
  watch: {
    total() {
      console.log("翻页器 变化");
      this.compute_maxPageNum();
    },
    pageSize() {
      this.compute_maxPageNum();
    },
    currentPage() {
      this.handle_currentPage_change();
    },

  },
  methods: {
    ...mapActions({
      set_pageSize: "set_pageSize"
    }),
    compute_maxPageNum() {
      console.log("翻页器 计算 最大值");

      this.maxPageNum = Math.ceil(this.total / this.pageSize);

      console.log(this.maxPageNum);
    },
    compute_pagination() {
      let obj = {
        page: this.currentPage,
        rowsPerPage: this.pageSize,
        rowsNumber: this.total
      };
      this.pagination = Object.assign(this.pagination, obj);
      this.$emit("handle_pagination_change", this.pagination);
    },

    initPageSizeAndCurrentPage(pageSize, currentPage) {
      if (this.get_pageSize !== pageSize) {
        this.set_pageSize(pageSize);
      }
      this.compute_pagination();

      if (Number.isNaN(Number(currentPage))) {
        currentPage = 1;
      }

      currentPage = currentPage <= 0 ? 1 : currentPage;
      currentPage =
        currentPage >= this.maxPageNum ? Number(this.maxPageNum) : currentPage;

      this.currentPage = Number(currentPage);

      this.$forceUpdate();

      if (this.tabledata_can_request) {
        // this.init_tabledata();
        // this.$emit('init_tabledata',this.currentPage,this.pageSize)
        // 在 抽离后的翻页组件内 当前页 传递父级， pagesize 走全局
        this.$emit("init_tabledata");
      }
    },

    handle_pageSize_change(val) {
      this.initPageSizeAndCurrentPage(val, this.currentPage);
    }
    // handle_currentPage_change(val) {
    //   console.log(val);

    //   this.initPageSizeAndCurrentPage(this.pageSize, val);
    // }
  }
};
