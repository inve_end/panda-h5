import { mapGetters, mapActions } from "vuex";
export default {
    created() {
       
    },
    mounted() {
        this.init_all();
    },
    computed: {
        ...mapGetters({
            accunt_info: "get_account",

        })
    },
    methods: {

        ...mapActions({
            set_account: "set_account",
        }),
        init_all() {
            this.init_base()
            return false
            this.check_accunt_info_and_token();
            if (this.accunt_info && !this.check_if_router_disabled()) {
                this.init_base();
            } else {
                this.$router.push({
                    name: "login"
                });
            }
        },
        check_accunt_info_and_token() {
            let accunt_info = this.$q.cookies.get("accunt_info");
            console.log(" ------------this.accunt_info-----------------");
            console.log(accunt_info);
            if (accunt_info) {
                this.set_account(JSON.parse(accunt_info));
            }

        },

        init_base() {
            this.init_tabledata();
        },
        init_tabledata() { },
        if_have_permission() {
            return true;
        },
        check_if_router_disabled() {
            // 验证权限  和是否禁止 访问
            // this.router_disabled;
            return false;
        }
    }
};