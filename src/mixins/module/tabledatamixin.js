import { mapGetters } from "vuex";

let exist_div;
export default {
  data() {
    return {
      currentPage: 1,
      queryform_form_changed: false,
      request_currentPage: -1,
      data_label: " ", // 此处设置无数据时候的显示值为空
      data_label_true: "Sorry，没有找到您想要的结果，请更换查询条件！"
    };
  },
  created() {
    this.request_currentPage = this.currentPage;
  },
  computed: {
    ...mapGetters({
      pageSize: "get_pageSize"
    })
  },
  methods: {
    compute_pagination() {
      let obj = {
        page: this.currentPage,
        rowsPerPage: this.pageSize,
        rowsNumber: this.total
      };
      this.pagination = Object.assign(this.pagination, obj);
      console.log("父级 表格    this.pagination");
      console.log(this.pagination);
    },
    restart_init_tabledata() {
      //  此方法 用处 是 在  查询条件 改变 重置 当前页数
      console.log("//  此方法 用处 是 在  查询条件 改变 重置 当前页数");
      // this.currentPage = 1;
      this.queryform_form_changed = !this.queryform_form_changed;
    },
    handle_pagination_change(val) {
      console.log("父级 表格接收到的  this.pagination=val ");
      console.log(val);
      this.pagination = val;
    },
    handle_currentPage_change(val) {
      console.log(val);
      // 接收到的 翻页器 当前页
      this.currentPage = val;
      // this.init_tabledata()
    },
    init_tabledata_before() {
      if (this.queryform_form_changed) {
        this.currentPage = 1;
      }
      this.compute_pagination();
      this.$nextTick(() => {this.change_data_label_style(2)});
      this.tabledata_loading = true;
      this.tabledata_can_request = false;
    },
    resolve_tabledata_response_base(res) {
      this.init_tabledata_after();
      this.tabledata =
        this.find_value_in_obj_bypath(res, "res.data.data.records") || [];
      this.currentPage =
        this.find_value_in_obj_bypath(res, "res.data.page") * 1 || 1;
      this.total =
        this.find_value_in_obj_bypath(res, "res.data.total") * 1 || 0;
    },
    init_tabledata_after() {
      this.queryform_form_changed = false;
      this.tabledata_loading = false;
      this.tabledata_can_request = true;
      this.$nextTick(this.change_data_label_style);
    },
    // 改变查询没信息样式
    change_data_label_style(where = 1) {
      let nodata_total = document.getElementsByClassName(
        "q-table__bottom row items-center q-table__bottom--nodata"
      );
      let nodata = nodata_total && nodata_total[0];
      let first = nodata && nodata.firstElementChild;
      // 移除图标
      if (first && first.tagName === "I") {
        first.style.display = "none";
      }
      // 移除存在的图片元素
      exist_div && (exist_div.remove())
      // nodata是检验是否无数据的标志位，如果有，则增加icon图片
      if (nodata) {
        let exist_img = document.createElement("img");
        exist_img.setAttribute("src", "statics/svg/icon_empty.svg");
        // 图片的样式
        exist_img.style.cssText = `width: 60px; height: 60px; position: absolute; top: -67px; left: 95px`;
        // 主体元素样式
        exist_div = document.createElement('div')
        exist_div.style.cssText = `position: absolute; left: 871px; top: calc(266px + 67px);`;
        exist_div.append(exist_img, this.data_label_true)
        // 没数据查询时候字体颜色显示值设定
        if (where === 1) {
          nodata.classList.add("panda-text-grey");    
        } else {
          nodata.classList.add("text-panda-text-light");
        }
        nodata.appendChild(exist_div);
      }
    }
  }
};
