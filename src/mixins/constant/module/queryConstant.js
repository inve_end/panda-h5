import { mapGetters, mapActions } from "vuex";
export default {
  data() {
    return {

    };
  },
  computed: {
    ...mapGetters({
      dataSource_obj: "get_dataSource_obj",
      sport_region_constant: "get_sportRegion_array",
      sport_type_constant: "get_sportType_array"
    })
  },
  mounted() {
    this.init_query_constant();
  },

  methods: {

    ...mapActions({
      set_dataSource_obj: "set_dataSource_obj",
      set_sportRegion_array: "set_sportRegion_array"
      // set_sportType_array:'set_sportType_array',
    }),
    init_query_constant() {
      this.set_dataSource_obj();
      this.set_sportRegion_array();
      // this.set_sportType_array();
    },



  }
};
