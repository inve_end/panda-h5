import { mapGetters, mapActions } from "vuex";
export default {
  data() {
    return {
    };
  },
  computed: {
    ...mapGetters({
      dataSource_obj: "get_dataSource_obj",
    }),

  },
  methods: {
    compute_dataSourceCode_fullname(str) {
      if (!str) {
        return;
      }
      let obj = this.$lodash.find(this.dataSource_obj.raw_dataSource, el => {
        return el.code == str;
      });
      if (obj && obj.fullName) {
        return obj.fullName;
      }
      return str;
    },
    filter_value_to_label(name, value, valuekey, labelkey) {
      if (!name) {
        return "";
      }
      if (!this[`${name}_constant`]) {
        return "";
      }
      valuekey = valuekey ? valuekey : "value";
      labelkey = labelkey ? labelkey : "label";
      let str = "";
      this[`${name}_constant`].map(x => {
        if (x[valuekey] == Number(value)) {
          str = x[labelkey];
        }
      });
      return str;
    },
  }
};
