import { mapGetters, mapActions } from "vuex";
export default {
  data() {
    return {
      
      match_level_constant: [
        { label: "全部", value: "" },
        { label: "未评级", value: 0 },
        { label: "一级联赛", value: 1 },
        { label: "二级联赛", value: 2 },
        { label: "三级联赛", value: 3 },
        { label: "四级联赛", value: 4 },
        { label: "五级联赛", value: 5 }
      ],
      odds_status_constant: [
        { label: "全部", value: "" }, // first 传 空  second  ""
        { label: "Panda开盘", value: { source: "PA", open: 1 } },
        { label: "Panda封盘", value: { source: "PA", open: 2 } },
        { label: "SR开盘", value: { source: "SR", open: 1 } },
        { label: "SR封盘", value: { source: "SR", open: 2 } },
        { label: "BC开盘", value: { source: "BC", open: 1 } },
        { label: "BC封盘", value: { source: "BC", open: 2 } },
        { label: "未开盘", value: { source: "", open: 0 } }, // first 传 空  second 0
        { label: "无盘口数据", value: { source: "", open: 999 } } // first 传 空  second 不为空
      ]
    };
  },
  computed: {
    ...mapGetters({
      sport_region_constant: "get_sportRegion_array",
      sport_type_constant: "get_sportType_array"
    }),

  },
  methods: {
    filter_value_to_label(name, value, valuekey, labelkey) {
      if (!name) {
        return "";
      }
      if (!this[`${name}_constant`]) {
        return "";
      }
      valuekey = valuekey ? valuekey : "value";
      labelkey = labelkey ? labelkey : "label";
      let str = "";
      this[`${name}_constant`].map(x => {
        if (x[valuekey] == Number(value)) {
          str = x[labelkey];
        }
      });
      return str;
    },
    filterFn_sport_region_constant(val, update) {
      console.log(val);
      if (val === "") {
        update(() => {
          this.sport_region_constant_filtered = this.sport_region_constant;
        });
        return;
      }
      update(() => {
        const needle = val.toLowerCase();
        this.sport_region_constant_filtered = this.sport_region_constant.filter(
          v => v.introduction.toLowerCase().indexOf(needle) > -1
        );
      });
      console.log(this.sport_region_constant_filtered);
    }
  }
};
