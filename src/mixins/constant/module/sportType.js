import { mapGetters, mapActions } from "vuex";
import { sport_type_mapping_constant } from "src/config/mapping/sport_type.js"

export default {
    data() {
        return {
            // sport_type_constant: [
            //     { introduction: '足球', value: '1', enname: 'football' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },
            //     { introduction: '篮球', value: '2', enname: 'basketball' },

            // ],

            sport_type_mapping_constant: sport_type_mapping_constant,
        }
    },
    computed: {
        ...mapGetters({
            sport_type_constant: "get_sportType_array",
        }),
    },
    mounted() {

    },
    watch: {

    },
    methods: {
        ...mapActions({

            set_sportType_array: 'set_sportType_array',
            set_selectedApiModel: 'set_selectedApiModel',
            set_selectedSportTypeInfo: 'set_selectedSportTypeInfo',
        }),
        filter_sport_type_enname(val) {
            let str = ""
            this.sport_type_mapping_constant.map(x => {
                if (x.introduction == val) {
                    str = x.enname

                }
            })
            if (!str) {
                let msg = `<div>当前选择体育类型:  <span class="panda-text-blue q-mx-md">${val}</span>  不在前端代码球类参照表中。</div>
                <div>请联系开发人员核对修复!</div>`
                this.$q.dialog({
                    title: '',
                    message: msg,
                    html: true,
                    cancel: false,
                    persistent: true,
                    contentClass: '',
                    ok: {
                        label: "确定"
                    },
                    // cancel: {
                    //     color: "white",
                    //     label: "取消",
                    //     textColor: "black"
                    // }

                })

            }
            return str
        },



    },
}