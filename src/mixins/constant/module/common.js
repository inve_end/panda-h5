const dataSourceCode_name_mapping = {
  sr: "Sportradar",
  bc: "BetConsrtuct"
};
export default {
  data() {
    return {
      file_server_address:"http://161.117.194.247:7788/",
      image_panda_placeholder:'statics/image/panda_placeholder.jpg' 
    };
  },
  methods: {
    compute_image_src(str){
      // 61.117.194.247:7788/fastdfs/download/image?filePath=group1/M00/00/00/rBXolV1l74iARfYTAAAEjFwbBqQ646.png
      return   str? this.file_server_address+'fastdfs/download/image?filePath=' +str: this.image_panda_placeholder 
    }
  }
};
