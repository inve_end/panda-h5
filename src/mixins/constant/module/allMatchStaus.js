import { mapGetters } from "vuex";

export default {
    data() {
        return {
        }
    },
    computed: {
        ...mapGetters({
            // api_model_name:"get_selectedApiModel",
            allMatchStaus:'get_allMatchStaus'

        }),
    },
    methods: {
        compute_current_sporttype_matchStatus_show_text(num){ //num sprotId
            let arr=this.allMatchStaus[this.api_model_name]
            if(!arr){
                return num
            }else{
              let  obj =this.$lodash.find(arr,o=>{ return o.id==num})  
              return obj.value || ''
            }
        }
        
    },



}