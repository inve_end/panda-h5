export default {
  data() {
    return {
      //中文简体>简称>中文繁体>英文
      order_by: ["zs", "jc", "zh", "en"]
    };
  },
  methods: {
    all_language_rebase(
      item,
      keyname,
      dataSourceCodeKey,
      childrenkey,
      exclude = []
    ) {
      if (!item) {
        return [];
      }
      if (!item[keyname]) {
        return [];
      }
      if (!dataSourceCodeKey) {
        dataSourceCodeKey = "dataSourceCode";
      }
      if (!childrenkey) {
        childrenkey = "relatedTour";
      }

      /**
       * 作用 集成语言信息，输出固定配置结构
       * 传入 综合对象
       * 传入 挂在语言信息的字段名    例如     awayTeamNames
       * 传入 数据来源字段名          默认    dataSourceCode
       * 传入 子项目的字段名          默认    relatedTour
       */

      let language_name_base_config = {
        jc: {
          icon: "N",
          color: "red",
          key: "jc",
          title: "简称",
          info: []
        },
        zh: {
          icon: "繁",
          color: "green",
          key: "zh",
          title: "中文繁体",
          info: []
        },
        en: {
          icon: "En",
          color: "amber",
          key: "en",
          title: "英语",
          info: []
        },
        zs: {
          icon: "简",
          color: "blue",
          key: "zs",
          title: "中文简体",
          info: []
        }
      };

      let order_by = this.$lodash.pullAll([...this.order_by], exclude);

      let obj = this.$lodash.cloneDeep(language_name_base_config);
      // 先推入父级的
      if (item[keyname]) {
        let keys = Object.keys(item[keyname]);
        keys.map(y => {
          obj[y]["info"].push({
            source: "PMTS",
            value: item[keyname][y]
          });
        });
      }
      // 再 推入子集
      if (item[childrenkey]) {
        item[childrenkey].map(x => {
          let keys = Object.keys(x[keyname]);
          keys.map(y => {
            obj[y]["info"].push({
              source: x[dataSourceCodeKey],
              value: x[keyname][y]
            });
          });
        });
      }

      let arr = [];
      order_by.map(x => {
        if (obj[x].info.length > 0) {
          arr.push(obj[x]);
        }
      });
      return arr;
    },
    compute_table_item_show_name(item, namekey, alllanguagekey) {
      if (!item) {
        return "";
      }
      // 对应 只有 一级的 取值情况
      if (namekey && item[namekey]) {
        return item[namekey];
      }
      // 对应两层取值情况
      let return_str = "";
      if (alllanguagekey && item[alllanguagekey]) {
        for (let i = 0; i < this.order_by.length; i++) {
          let str = item[alllanguagekey][this.order_by[i]];
          if (str) {
            return_str = str;
            break;
          }
        }
      }

      return return_str;
    },
    compute_languagename_by_key(item,alllanguagekey,key='jc'){
        let return_str = "";
        if (alllanguagekey && item[alllanguagekey]) {
            return_str=  item[alllanguagekey][key] ||''
        }
        return return_str;
    }

  }
};
