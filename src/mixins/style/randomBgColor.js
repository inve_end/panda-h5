export default {
  data() {
    return {
      random_color_pool: [
        "#C17485",
        "#CDA567",
        "#6685B8",
        "#8E9C3F",
        "#946DB4",
        "#8D99AE",
        "#73A878"
      ]
    };
  },
  methods: {}
};
