/**
 * 声明  开发参照屏幕宽度 1920
 */
export default {
  data() {
    return {};
  },

  methods: {
    rebuild_tablecolumns_config(tablecolumns_config) {
      let window_width_reference = 1745 - 60;
      let ww = window.innerWidth - 60;
      let fixedwdth = 0;
      let unfixedwidth = 0;
      tablecolumns_config.map(x => {
        if (x.width == 0) {
          let message =
            " 注意   当前使用的 表格宽度样式逻辑，必须保证有默认宽度设置！";

          this.$q.notify({
            message: `${message}`,
            color: "negative"
          });
          x.width = 100;
        }
        if (x.fixed == 1) {
          fixedwdth += x.width;
        } else {
          unfixedwidth += x.width;
        }
      });
      console.log("表格宽度 计算");
      console.log("定宽    :   " + fixedwdth);
      console.log("不定宽    :   " + unfixedwidth);
      //  实际的 参数配置总和 可能并不等于 1920 -60  需要 计算  不定宽部分实际使用宽度与设置数值之间比率
      let rate =
        ((window_width_reference - fixedwdth) / unfixedwidth) *
        (ww / window_width_reference);

      tablecolumns_config.map(x => {
        let obj = {
          // width:'px',
          // maxWidth:'px',
          // minWidth:'px',
        };
        // fixed: 1, width: 30, min: 30
        if (x.fixed == 1) {
          obj = {
            width: x.width + "px",
            maxWidth: x.width + "px",
            minWidth: x.width + "px"
          };
        } else {
          obj = {
            width: x.width * rate + "px",
            maxWidth: x.width * rate + "px",
            minWidth: x.width * rate + "px"
          };
        }
        x.style = obj;
      });

      console.log("加工后的 表格宽度配置表 ");
      console.log(tablecolumns_config);
      return tablecolumns_config;
    },

    // 传入表格的索引
    _compute_table_col_width(tablecolumns_config, name) {
      // 需要引入
      let window_width_reference = 1920;
      let ww = window.innerWidth;

      let col_n_config = this.$lodash.find(tablecolumns_config, o => {
        return o.name == name;
      });
      // {width:110,min:110},
      if (col_n_config) {
        if (col_n_config.width == 0) {
          return `width:auto; !important`;
          // return `width:auto; min-width:${col_n_config.min}px;`;
        } else {
          if (col_n_config.fixed == 1) {
            return `width:${col_n_config.width}px; max-width:${col_n_config.width}px; !important`;
          } else {
            let real_w = parseInt(
              (col_n_config.width / window_width_reference) * ww
            );

            return `width:${real_w}px; max-width:${real_w}px !important`;
          }

          // return `width:${real_w}px; min-width:${col_n_config.min}px;`;
        }
      } else {
        return `width:auto; `;
      }
    },
    _compute_table_col_width(tablecolumns_config, name) {
      // 需要引入
      let window_width_reference = 1920;
      let ww = window.innerWidth;

      let col_n_config = this.$lodash.find(tablecolumns_config, o => {
        return o.name == name;
      });

      if (col_n_config) {
        return col_n_config.style;
      } else {
        let message = " 注意 你的表格列表 索引名称，在配置项中并不存在";

        this.$q.notify({
          message: `${message}`,
          color: "negative"
        });
      }
    },

    _compute_table_col_width_return_num(tablecolumns_config, name, str, num) {
      // 需要引入
      let window_width_reference = 1920;
      let ww = window.innerWidth;
      let col_n_config = this.$lodash.find(tablecolumns_config, o => {
        return o.name == name;
      });
      let return_width = 100;
      // {width:110,min:110},
      if (col_n_config) {
        if (col_n_config.width == 0) {
          // return `width:auto; !important`;
          // return `width:auto; min-width:${col_n_config.min}px;`;
          return_width = 100;
        } else {
          if (col_n_config.fixed == 1) {
            // return `width:${col_n_config.width}px; !important`;
            return_width = col_n_config.width;
          } else {
            return_width = parseInt(
              (col_n_config.width / window_width_reference) * ww
            );

            // return `width:${real_w}px; !important`;
          }
        }
      }

      return return_width;
    },

    compute_table_col_width(name) {
      return this._compute_table_col_width(this.tablecolumns, name);
    }
  }
};
