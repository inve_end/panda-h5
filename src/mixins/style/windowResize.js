/**
 * 因为 panda 项目 目标是 输出 PC 端 exe 软件 界面 效果 
 * 特点是 是 浏览器高度 或者 视窗高度定高 ，内部滚动 
 * 产生问题是 每次 视口大小改变 界面就需要 重构 此时需要监听 浏览器事件 
 */
import { mapGetters } from "vuex";
 export default {

    data(){
        return {

        }
    },
    computed: {
        ...mapGetters({
            window_innerHeight:'get_window_innerHeight',
            window_innerWidth:'get_window_innerWidth',
        })
    },
    mounted() {
        this.init_all_computed_style()
        
    },
    watch: {
        window_innerHeight(){
            this.when_window_innerHeight_change()
        },
        window_innerWidth(){ 
            this.when_window_innerWidth_change()
         },
    },
    methods: {
        init_all_computed_style(){},
        when_window_innerHeight_change(){},
        when_window_innerWidth_change(){},
        
    },
    
 }