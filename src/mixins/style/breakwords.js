import tableColWidth from "src/mixins/style/tableColWidth.js";
export default {
  mixins: [tableColWidth],
  data() {
    return {};
  },
  methods: {
    break_words_to_mulit_line(tablecolumns_config, name, str, num) {
      /**
       * 传入 表格各列 配置
       * 传入 当前 col 的名称
       * 传入 当前需要截取的字符串
       * 传入 打断参考值
       *
       */
      // 计算当前 单元格 宽度
      let return_str = "";
      let arr = this.break_words_to_other_base(
        tablecolumns_config,
        name,
        str,
        num
      );
      arr.map(x => {
        return_str += `<div>${x}</div>`;
      });

      return return_str;
    },
    // 返回包裹文字元素
    break_words_to_ellipsis(tablecolumns_config, name, str) {

      if (!str) {
        return "";
      }
      let return_str = `<span>${str}</span>`  
      return return_str
    },

    break_words_to_other_base(tablecolumns_config, name, str, num) {
      /**
       * 传入 表格各列 配置
       * 传入 当前 col 的名称
       * 传入 当前需要截取的字符串
       * 传入 打断参考值
       *
       */
      // 计算当前 单元格 宽度
      let return_arr = [];

      if (!str) {
        return [""];
      }
      num = num || 12;
      if (str.length < num) {
        return [str];
      }

      let col_width = this._compute_table_col_width_return_num(
        tablecolumns_config,
        name
      );
      col_width = col_width - 20;
      col_width = col_width < 30 ? 30 : col_width;

      let arr = [];

      let pattern_zn = new RegExp("[\u4E00-\u9FA5]+");
      // let pattern_en = new RegExp("[A-Za-z]+");

      if (pattern_zn.test(str)) {
        // 中文
        num = Math.ceil(col_width / 14);
      } else {
        num = Math.ceil(col_width / 7);
      }
      let chunk = Math.ceil(str.length / num);
      for (let i = 0; i < chunk; i++) {
        arr.push(str.slice(i * num, (i + 1) * num));
      }

      // let return_str = "";

      arr.map(x => {
        // return_str += `<div>${x}</div>`;
        return_arr.push(`${x}`);
      });

      return return_arr;
      //   return arr;
    },

    break_words_to_mulit_line_by_value(str, num = 7) {
      if (!str) return "";
      let chunk = Math.ceil(str.length / num);
      let arr = [],
        return_str = "";
      for (let i = 0; i < chunk; i++) {
        arr.push(str.slice(i * num, (i + 1) * num));
      }
      arr.map(x => {
        return_str += `<div>${x}</div>`;
      });
      return return_str;
    }
  }
};
