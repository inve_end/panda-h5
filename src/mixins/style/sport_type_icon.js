/**
 * 此文件 主要是用于 给 球类 挂载 icon
 *
 * 修改人: @jinnian
 * 注释时间: 2019-09-07 13:26:01
 */

import {sport_type_mapping_constant} from "src/config/mapping/sport_type.js";
export default {
  data() {
    return {
      sport_type_mapping_constant: sport_type_mapping_constant
    };
  },
  filters: {
    sport_type_icon: function(value) {
      let obj = this.$lodash.find(sport_type_mapping_constant, o => {
        return o.name == value;
      });
      return obj ? obj.icon : "";
    }
  },
  methods: {
    compute_sport_type_icon(value) {
      let obj = this.$lodash.find(sport_type_mapping_constant, o => {
        return o.name == value;
      });
      return obj ? obj.icon : "";
    }
  }
};
