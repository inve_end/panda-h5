import { mapGetters } from "vuex";

export default {
    data() {
        return {
        }
    },
    computed: {
        ...mapGetters({
            socket_data:'get_socket_data',
            socket_data_by_sport: 'get_socket_data_by_sport'
        }),
    },
    methods: {
        compute_socket_data( ){ 
            console.log(this.socket_data);
        }
        
    },



}