
import * as api_admin from "src/api/module/admin/index.js"
import * as api_common from "src/api/module/common/index.js"
import * as api_global from "src/api/module/global/index.js"


export {

    api_admin,
    api_common,
    api_global

}





