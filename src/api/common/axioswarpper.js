/**
 * http配置
 */
import axios from "axios";

// 超时时间
axios.defaults.timeout = 15000;

// http请求拦截器

// 在向后端请求时，如果上传的数据里存在file文件对象，需要用到表单提交，这时候我们需要将JSON对象，转成formData对象，具体见代码

// const formData = new FormData();
// Object.keys(params).forEach((key) => {
// formData.append(key, params[key]);
// });

// 下面也有可能需要formData转JSON，代码如下：

// var jsonData = {};
// formData.forEach((value, key) => jsonData[key] = value);

axios.interceptors.request.use(
  config => {
    console.log(config);
    // 自定义参数 说明  type  1 formdata 2 json 3 file   默认 json
    if (config.type == 1) {
        const formData = new FormData();
        //   let data = JSON.parse(config.data);
          let data = config.data;
          Object.keys(data).forEach(key => {
            formData.append(key, data[key]);
          });
          config.data =formData;
      config.headers["Content-Type"] =
        "application/x-www-form-urlencoded; charset=UTF-8";

    } else if (config.type == 3) {

      config.headers["Content-Type"] = "multipart/form-data";
    }

    return config;
  },
  error => {
    return Promise.reject(error);
  }
);
// http响应拦截器
axios.interceptors.response.use(
  res => {
    // 响应成功关闭loading

    console.log(res);
    // res.data.code
    return res;
  },
  error => {
    return Promise.reject(error);
  }
);

export default axios;
