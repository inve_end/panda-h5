import axios from 'src/api/common/axioswarpper.js'
import * as mockdata from 'src/mock/mockv1.1.js'
let prefix = process.env.API_PREFIX_JOB;
export const post1 = (params, url) => axios.post(`${prefix}${url}`, params)
export const get1 = (params, url) => axios.get(`${prefix}${url}`, { params: { ...params } })




// /sportType/getSportType   获取联赛级别以及赛事状态接口


// export const get_sportType_page = (params, url="/sportType/page") => axios.get(`${prefix}${url}`, { params: { ...params } })
//获取体育类型接口
export const get_sportType_page = (params, url="/sportType/page") =>new Promise((resolve, reject) => {
  resolve({data:mockdata.sport_type });

}) 
// 获取玩法详情接口
export const get_betinfo = (params, url="/sportType/page") => new Promise((resolve, reject) => {
  resolve({data:mockdata.betinfo});

})
//获取联赛数据接口
export const get_tournamentlist = (params, url="/sportType/page") =>new Promise((resolve, reject) => {
  resolve({data:mockdata.tournamentlist});

}) 

//获取赛事数据接口
export const get_matchInfo = (params, url="/sportType/page") => new Promise((resolve, reject) => {
  resolve({data:mockdata.matchInfo});

})









//栏目赛事查询接口(sklee)
export const post_mainpage_matchList = (params, url="/matchInfo/matchList") => axios.post(`${prefix}${url}`, params)


// 根据玩法查询盘口信息（christion）
export const get_tournament_odds_info = (params, url="/matchDetail/getMatchOddsInfo") =>axios.get(`${prefix}${url}`, { params: { ...params } })






//菜单栏目初始化接口
  export const get_menu_init = (params, url="/menu/init") =>axios.get(`${prefix}${url}`, { params: { ...params } })

//菜单栏目加载赛事统计接口
export const get_menu_count = (params, url="/menu/count") =>axios.get(`${prefix}${url}`, { params: { ...params } })

//赛事详情页面接口
export const get_matchDetail_MatchInfo = (params, url="/matchDetail/getMatchDetail") =>axios.get(`${prefix}${url}`, { params: { ...params } })


//根据玩法查询盘口信息
export const get_matchDetail_getMatchOddsInfo = (params, url="/matchDetail/getMatchOddsInfo") => axios.get(`${prefix}${url}`, { params: { ...params } })







//获取标准比赛玩法类型信息接口(kiu)
export const get_tournament_playtype = (params, url="/tournament/getType") =>axios.get(`${prefix}${url}`, { params: { ...params } })

//获取标准联赛统计玩法接口（kiu）
export const get_tournament_getCount = (params, url="/tournament/getCount") =>axios.get(`${prefix}${url}`, { params: { ...params } })


// 获取详情页面玩法集接口（christion）
//   /category/getCategoryList
export const get_category_getCategoryList = (params, url="/category/getCategoryList") =>axios.get(`${prefix}${url}`, { params: { ...params } })



