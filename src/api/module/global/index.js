
import axios from 'src/api/common/axioswarpper.js'
import * as mockdata from 'src/mock/mockv1.1.js'
let prefix = process.env.API_PREFIX_JOB;
export const post1 = (params, url) => axios.post(`${prefix}${url}`, params)
export const get1 = (params, url) => axios.get(`${prefix}${url}`, { params: { ...params } })



//获取联赛级别以及赛事状态(kiu)
export const get_sporttype_main_information = (params, url="/sportType/getSportType") =>axios.get(`${prefix}${url}`, { params: { ...params } })

