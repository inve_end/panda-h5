package com.panda.sports.manager.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.panda.sports.manager.entity.StandardMatchInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author Mirro
 * @Project Name :  panda_data_service
 * @Package Name :  com.panda.sports.manager.vo
 * @Description:
 * @date 2019/9/5 11:38
 * @ModificationHistory Who    When    What
 */
@Data
@ApiModel(description = "赛事数据传输模型")
public class MatchVO extends StandardMatchInfo {
    /**
     * 保存多语言信息  key 语言类型    value 文本信息
     **/
    @ApiModelProperty("所属联赛名称")
    private Map<String , String> leagueName;

    @ApiModelProperty("关联第三方赛事")
    private List<ThirdMatchVo> relatedMatch;

    @ApiModelProperty(value = "标准数据的数据来源(true:是；false:否)")
    private Boolean standard;

    @ApiModelProperty(value = "主场队ID")
    private Long homeTeamId;

    @ApiModelProperty(value = "主场队名称")
    private Map<String, String> homeTeamNames;

    @ApiModelProperty(value = "主场队log连接地址")
    private String homeTeamLogUrl;

    @ApiModelProperty(value = "主场队log缩略图连接地址")
    private String homeTeamLogUrlThumb;

    @ApiModelProperty(value = "客场队ID")
    private Long awayTeamId;

    @ApiModelProperty(value = "客场队名称")
    private Map<String, String> awayTeamNames;

    @ApiModelProperty(value = "客场队log连接地址")
    private String awayTeamLogUrl;

    @ApiModelProperty(value = "客场队log缩略图连接地址")
    private String awayTeamLogUrlThumb;

    @JsonSerialize(using= ToStringSerializer.class)
    @ApiModelProperty(value = "联赛namecode")
    private Long tournamentNameCode;

    @JsonSerialize(using= ToStringSerializer.class)
    @ApiModelProperty(value = "主队namecode")
    private Long homeNameCode;

    @JsonSerialize(using= ToStringSerializer.class)
    @ApiModelProperty(value = "客队namecode")
    private Long awayNameCode;

}
