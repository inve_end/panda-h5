package com.panda.sports.bss.spi.schedule.dto;

import com.panda.sports.bss.common.base.BaseVO;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 赛事盘口交易项表
 * </p>
 *
 * @author CodeGenerator
 * @since 2019-09-03
 */
@Data
public class StandardSportMarketOddsDTO  extends BaseVO {

    /**
     * 表ID，自增
     */
    private Long id;

    /**
     * 所属联赛ID    standard_sport_tournament.id
     */
    private Long standardTournamentId;

    /**
     * 标准比赛id
     */
    private Long standardMatchId;

    /**
     * 盘口ID  standard_sport_market.id
     */
    private Long marketId;

    /**
     * 投注项模板Id
     */
    private Long oddsFieldsTempletId;

    /**
     * 1 锁盘； 0 不锁盘。锁盘后用户不可下注
     */
    private Integer operateLock;

    /**
     * 当前交易项是否被激活.1激活；0未激活(锁盘)
     */
    private Integer active;

    /**
     * 名称编码. 用于多语言。交易项可能有也可能没有该字段。需要的时候填入
     */
    private Long nameCode;

    /**
     * 名称,V1.2统一命名规则。
     */
    private String name;

    /**
     * 该交易项在客户端显示的值，空则不显示
     */
    private String showsValue;

    /**
     * 交易项赔率。单位：0.0001
     */
    private Integer fieldOddsValue;

    /**
     * 例如：under ，over  ，odd， even等
     */
    private String checkMethod;

    /**
     * 1：可见； 0：不可见
     */
    private Integer visible;

    /**
     * 用于排序，大于1，越小越靠前
     */
    private Integer orderOdds;

    /**
     * 当前交易项更新时间
     */
    private Long updateTime;

    /**
     * 取值： SR BC分别代表：SportRadar、FeedConstruc。详情见data_source
     */
    private String dataSourceCode;

    /**
     * 该字段用于做风控时，需要替换成风控服务商提供的交易项id。 如果数据源发生切换，当前字段需要更新。
     */
    private String thirdOddsFieldSourceId;

    /**
     * 是否需要人工确认开奖。1 需要； 0 不需要
     */
    private Integer managerConfirmPrize;

    /**
     * UTC.该交易项从该字段指定的时间开始接受下注。
     */
    private Long startTime;

    /**
     * UTC.该交易项从该字段指定的时间停止接受下注。
     */
    private Long endTime;

    private String remark;

    private Long createTime;

    private Long modifyTime;

}
