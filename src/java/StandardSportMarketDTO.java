package com.panda.sports.bss.spi.schedule.dto;


import com.panda.sports.bss.common.base.BaseVO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/** * <p>
 * 足球赛事盘口表. 使用盘口关联的功能存在以下假设：同一个盘口的显示值不可变更，如果变更需要删除2个盘口之间的关联关系。。
 * </p>
 *
 * @author CodeGenerator
 * @since 2019-09-03
 */
@Data
public class StandardSportMarketDTO  extends BaseVO {

    /**
     * 数据库id，自增
     */
    private Long id;

    /**
     * 运动种类id。 对应表 sport.id
     */
    private Long sportId;

    /**
     * 所属联赛ID    standard_sport_tournament.id
     */
    private Long standardTournamentId;

    /**
     * 标准比赛ID   standard_match_info.id
     */
    private Long standardMatchInfoId;

    /**
     * 标准玩法id   standard_sport_market_category.id
     */
    private Long marketCategoryId;

    /**
     * 盘口类型。属于赛前盘或者滚球盘。1：赛前盘；0：滚球盘。
     */
    private Integer marketType;

    /**
     * 盘口名称编码. 用于多语言
     */
    private Long nameCode;

    /**
     * 该盘口具体显示的值。例如：大小球中，大小界限是： 3.5
     */
    private String oddsValue;

    /**
     * 盘口名称,V1.2统一命名规则。
     */
    private String oddsName;

    /**
     * 取值： SR BC分别代表：SportRadar、FeedConstruc。详情见data_source
     */
    private String dataSourceCode;

    /**
     * 1：可见； 0：不可见
     */
    private Integer visible;

    /**
     * 是否被结算结束。1 结算结束； 0 未结束
     */
    private Integer settled;

    /**
     * 当前玩法是否被激活.1激活；0未激活(锁盘)
     */
    private Integer active;

    /**
     * 当前玩法是否被取消..1取消；0未取消
     */
    private Integer canceled;

    /**
     * 1 锁盘； 0 不锁盘。锁盘后，用户不可下注。
     */
    private Integer operateLock;

    /**
     * 1 需要； 0 不需要
     */
    private Integer managerConfirmPrize;

    /**
     * 赔率变更时间.UTC时间
     */
    private Long changedTime;

    /**
     * 接收到第三方数据后，可以通过该字段快速定位到当前的盘口。通过玩法和具体内容确认盘口的唯一性。 SR提供的盘口数据id 生成算法：Type_Typeid_Subtypeid_Specialoddsvalue
     */
    private String thirdOddsType;

    /**
     * 该字段用于做风控时，需要替换成风控服务商提供的盘口id。 如果数据源发生切换，当前字段需要更新。
     */
    private String thirdOddsSourceId;

    /**
     * UTC.该盘口从该字段指定的时间开始接受下注。创建盘口数据时，从盘口配置表中读取并计算，
     */
    private Long startTime;

    /**
     * UTC.该盘口从该字段指定的时间停止接受下注。创建盘口数据时，从盘口配置表中读取并计算，
     */
    private Long endTime;

    private String remark;

    private Long createTime;

    private Long modifyTime;

    /**
     * 盘口投注项
     */
    private List<StandardSportMarketOddsDTO> marketOddsList;

}
