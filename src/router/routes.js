const routes = [
  {
    path: "/",
    redirect: { name: "odds" },
    component: () => import("src/layouts/MyLayout.vue"),
    children: [
      {
        path: "odds",
        name: "odds",
        redirect: { name: "odds_mainone" },
        component: () => import("src/pages/odds/index.vue"),
        children: [
          {
            path: "mainone",
            name: "odds_mainone",
            component: () => import("src/pages/odds/mainone/index.vue")
          },
          {
            path: "maintwo",
            name: "odds_maintwo",
            component: () => import("src/pages/odds/maintwo/index.vue")
          },

        ]
      },
      {
        path: "oddinfo",
        name: "odds_info",
        component: () => import("src/pages/odds/info/index.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("src/pages/error/Error404.vue")
  });
}

export default routes;
