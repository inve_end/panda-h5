import Vue from "vue";
import Vuex from "vuex";
// common
import accountStore from "src/store/module/common/accountStore.js";

//topmenu
import topmenu  from "src/store/module/topmenu/topmenu.js"
import menu_count  from "src/store/module/topmenu/menu_count.js"
import threemenu  from "src/store/module/topmenu/threemenu.js"

// header

import  toptitle from "src/store/module/header/toptitle.js"

// login
import  toshowlogin  from "src/store/module/login/toshowlogin.js"
// global
import  allMatchStaus from "src/store/module/global/allMatchStaus.js"
//socket
import socket from  "src/store/module/socket/socket.js"

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: [
      accountStore,
      topmenu ,
      threemenu,
      menu_count,
      toptitle,
      toshowlogin,
      allMatchStaus,
      socket
    ],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
