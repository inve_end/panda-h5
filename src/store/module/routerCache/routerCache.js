let map = (new Map()).set('usecache', 0) 
// 路由对象的提交参数表
export default {
  state: {
    // 缓存路由页面的提交数据数组
    router_cache: map
  },
  getters: {
    get_router_cache (state) {
      return state.router_cache
    },
    // 是否使用路由缓存
    get_use_router_cache (state) {
      return state.router_cache.get('usecache')
    }
  },
  mutations: {
    set_router_cache (state, router_cache) {
      state.router_cache.set(router_cache.name, router_cache.params)
    },
    set_use_router_cache (state, value) {
      state.router_cache.set('usecache', value)
    }
  },
  actions: {
    set_router_cache ({ commit }, router_cache) {
      commit('set_router_cache', router_cache)
    },
    set_use_router_cache ({ commit }, value) {
      commit('set_use_router_cache', value)
    }
  }     
}