/**
 * 头部的详情页面 标题那里 存在分级
 * 传入 数组 [ '','']
 * 索引1的位置无值则
 * 
 */
export default {
    // namespaced: true,
    state: {
        top_title: [],
    },
    getters: {
        get_top_title(state) {
            return state.top_title;
        }
    },
    actions: {
        set_top_title({ commit }, top_title) {
            commit("set_top_title", top_title)
        }

    },
    mutations: {
        set_top_title(state, top_title) {
            state.top_title = top_title
        }

    }
}