export default {
  state: {
    socket_data: "",
    bet_socket_data: "",
    socket_data_by_sport_id: {}
  },
  getters: {
    get_socket_data(state) {
      return state.socket_data;
    },
    get_bet_socket_data(state) {
      return state.bet_socket_data;
    },
    get_socket_data_by_sport: state => state.socket_data_by_sport_id
  },
  actions: {
    set_socket_data({ commit }, socket_data) {
      commit("set_socket", socket_data);
      commit("set_socket_data_by_sport", socket_data);
    },
    set_bet_socket_data({ commit }, bet_socket_data) {
      commit("set_bet_socket", bet_socket_data);
    }
  },
  mutations: {
    set_socket(state, socket_data) {
      state.socket_data = socket_data;
    },
    set_bet_socket(state, bet_socket_data) {
      state.bet_socket_data = bet_socket_data;
    },
    set_socket_data_by_sport(state, socket_data) {
      const { commandData } = socket_data;
      let mount_obj = {};
      if (commandData && Array.isArray(commandData)) {
        commandData.forEach(x => {
          if (x && typeof x == "object") {
            mount_obj[x.sportId] = x;
          }
        });
      }
      state.socket_data_by_sport_id = mount_obj;
    }
  }
};
