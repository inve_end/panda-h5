/**
 * 应对与 所有球类的 赛事状态
 * 因为每种赛事状态称呼不同，必须全局统一入口
 */
import { api_global } from "src/api/index.js";


export default {
  // namespaced: true,
  state: {
    allMatchStaus: {},
    // current_match_level_constant: [],
    // current_match_status_constant: []
  },
  getters: {
    get_allMatchStaus(state) {
      return state.allMatchStaus;
    },
    get_current_match_level_constant(state) {
      return state.current_match_level_constant;
    },
    get_current_match_status_constant(state) {
      return state.current_match_status_constant;
    }

  },
  actions: {
    set_allMatchStaus({ commit, state }, { sportId, key, arr }) {
      state.allMatchStaus[`sportId${sportId}`] =
        state.allMatchStaus[`sportId${sportId}`] || {};
      state.allMatchStaus[`sportId${sportId}`][key] =
        state.allMatchStaus[`sportId${sportId}`][key] || [];
      commit("set_allMatchStaus", { sportId, key, arr });
    },
    set_match_data({ commit, state, dispatch }, params) {
      // this.set_match_data({ id: id });
      /**
       * 只有点击事件会 触发这里
       * 因此 不论 有没有缓存  页面和 球类关联紧密的 信息都在此 更新
       * 
       */
      let matchPeriod_arr=[]
      let tournamentLevel_arr=[]
      console.log('set_match_data({ commit, state, dispatch }, params)' );
      console.log( state.allMatchStaus);
      console.log(params );

      if (!state.allMatchStaus[`sportId${params.id}`]) {
    

        api_global.get_sporttype_main_information(params).then(res => { //查找到当前接口 修改url  
        const {
          data: { code }
        } = res;
        if (code == 200) {
          const {
            data: {
              data: { matchPeriod, tournamentLevel }
            }
          } = res;
          function returnValue(params) {
            let arr = [];
            if (Array.isArray(params) && params.length > 0) {
              arr = [...params];
              arr.unshift({
                value: "",
                description: "全部"
              });
            }
            return arr;
          }
           matchPeriod_arr= returnValue(matchPeriod)
           tournamentLevel_arr= returnValue(tournamentLevel)

          dispatch("set_allMatchStaus", {
            sportId: params.keyWord,
            key: "match_status_constant",
            arr: matchPeriod_arr
          });
          dispatch("set_allMatchStaus", {
            sportId: params.keyWord,
            key: "match_level_constant",
            arr: tournamentLevel_arr
          });

        }
      });
    }

      

    }
  },
  mutations: {
    set_allMatchStaus(state, { sportId, key, arr }) {
      if(!state.allMatchStaus[`sportId${sportId}`]){
        state.allMatchStaus[`sportId${sportId}`]={
          match_status_constant:[],
          match_level_constant:[]

        }
      }
      state.allMatchStaus[`sportId${sportId}`][key]=arr

    },
 
  }
};
