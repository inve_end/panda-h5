export default {
    // namespaced: true,
    state: {
        goback: '',
    },
    getters: {
        get_goback(state) {
            return state.goback;
        }
    },
    actions: {
        set_goback({ commit }, goback) {
            commit("set_goback", goback)
        }

    },
    mutations: {
        set_goback(state, goback) {
            state.goback = goback
        }

    }
}