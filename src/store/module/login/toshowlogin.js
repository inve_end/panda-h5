/**
 * 派发 显示登录弹窗事件  layout  组件内
 * 也 可以用  provide 的形式来处理 此事
 * 但是 正常情况下  常规都是 用  store
 * 修改人: @jinnian
 * 注释时间: 2019-09-07 09:53:17
 * 
 */


export default {
    // namespaced: true,
    state: {
        // to_show_login: true,
        to_show_login: false,
    },
    getters: {
        get_to_show_login(state) {
            return state.to_show_login;
        }
    },
    actions: {
        set_to_show_login({ commit }, to_show_login) {
            commit("set_to_show_login", to_show_login)
        }

    },
    mutations: {
        set_to_show_login(state, to_show_login) {
            state.to_show_login = to_show_login
        }

    }
}