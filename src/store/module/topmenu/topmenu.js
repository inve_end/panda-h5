export default {
    // namespaced: true,
    state: {
        topmenu: '',
    },
    getters: {
        get_topmenu(state) {
            return state.topmenu;
        }
    },
    actions: {
        set_topmenu({ commit }, topmenu) {
            commit("set_topmenu", topmenu)
        }

    },
    mutations: {
        set_topmenu(state, topmenu) {
            state.topmenu = topmenu
        }

    }
}