export default {
    // namespaced: true,
    state: {
        threemenu: {},
      test: ''
    },
    getters: {
        get_threemenu(state) {
            return state.threemenu;
        },
      get_test (state) {
          return state.test
      }
    },
    actions: {
        set_threemenu({ commit }, threemenu) {
            commit("set_threemenu", threemenu)
        }

    },
    mutations: {
        set_threemenu(state, threemenu) {
          state.threemenu = Object.assign({}, state.threemenu, threemenu)
          state.test = threemenu.name
        }
    }
}
