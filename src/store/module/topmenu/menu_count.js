export default {
    // namespaced: true,
    state: {
        menu_count: '',
    },
    getters: {
        get_menu_count(state) {
            return state.menu_count;
        }
    },
    actions: {
        set_menu_count({ commit }, menu_count) {
            commit("set_menu_count", menu_count)
        }

    },
    mutations: {
        set_menu_count(state, menu_count) {
            state.menu_count = menu_count
        }

    }
}