export default {
    // namespaced: true,
    state: {
        selectedSportTypeInfo: '',
        selectedApiModel:'football',
    },
    getters: {
        get_selectedSportTypeInfo(state) {
            return state.selectedSportTypeInfo;
        },
        get_selectedApiModel(state) {
            return state.selectedApiModel;
        }
    },
    actions: {
        set_selectedSportTypeInfo({ commit }, selectedSportTypeInfo) {
            commit("set_selectedSportTypeInfo", selectedSportTypeInfo)
        },
        set_selectedApiModel({ commit }, selectedApiModel) {
            commit("set_selectedApiModel", selectedApiModel)
        }

    },
    mutations: {
        set_selectedSportTypeInfo(state, selectedSportTypeInfo) {
            state.selectedSportTypeInfo = selectedSportTypeInfo
        },
        set_selectedApiModel(state, selectedApiModel) {
            state.selectedApiModel = selectedApiModel
        }

    }
}