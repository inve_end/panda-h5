import { api_common } from "src/api/index.js";
import lodash from "lodash";

//  此文件对应 赛程管理 v1.0 的 体育类型 数据来源 地区 作为公共搜索条件
//  联赛级别，前端 常量文件 写死
//  预留 强制执行 action  API  方便 后期 扩展对基础数据的 CRUD  ，目前均为只读内容

export default {
  // namespaced: true,

  state: {
    dataSource_obj: {
      raw_dataSource: [],
      is_commerce_dataSource: [],
      not_commerce_dataSource: [],
      data_sources_constant_commerce: []
    },
  },
  getters: {
    get_dataSource_obj(state) {
      return state.dataSource_obj;
    },
  },
  actions: {
    set_dataSource_obj({ state, dispatch, commit }) {
      // 常规 获取 ，有数值 就不再执行
      return dispatch("set_dataSource_obj_force");
    },
    set_dataSource_obj_force({ commit }) {
      //强制获取， 强制获取最新数据 进行更新
      api_common.get_dataSource_page({}).then(res => {
        let data = lodash.get(res, "data.data");
        let dataSource_obj = {}
        console.log("数据提取");
        console.log(data);
        if (data) {
          // 需要前置处理
          //  联赛数据来源编码列表. SR BC    MA:已匹配；UMA:未匹配；其余值: 全部
          //   code: "SR"
          //   commerce: 1
          //   createTime: 1564997479754
          //   fullName: "SportRadar"
          //   id: 1
          //   modifyTime: 1564997479754
          //   priority: 9
          //   remark: "以此为准，购买的第一数据源"
          //  需要自己添加    MA:已匹配；UMA:未匹配；其余值: 全部
          let arr = [
            { fullName: "已匹配", code: "MA" },
            { fullName: "未匹配", code: "UMA" }
          ];
          // 过滤非商业数据
          data = arr.concat(data);
          dataSource_obj = {
            raw_dataSource: data.concat([]),
            is_commerce_dataSource: data.concat([]).filter(el => el.commerce === 1).map(el => el.code), // 元素字符串
            not_commerce_dataSource: data.concat([]).filter(el => el.commerce === 0).map(el => el.code),
            data_sources_constant_commerce: data.concat([]).filter(el => el.commerce !== 0), // 元素对象
            data_sources_constant_not_pa: data.concat([]).filter(el => el.code !== 'PA')
          },
          commit("set_dataSource_obj", dataSource_obj);
        }
      });
    },
  },
  mutations: {
    set_dataSource_obj(state, dataSource_obj) {
      state.dataSource_obj = dataSource_obj;
    },
  }
};
