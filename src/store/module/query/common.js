import { api_common } from "src/api/index.js";
import lodash from "lodash";

//  此文件对应 赛程管理 v1.0 的 体育类型 数据来源 地区 作为公共搜索条件
//  联赛级别，前端 常量文件 写死
//  预留 强制执行 action  API  方便 后期 扩展对基础数据的 CRUD  ，目前均为只读内容

export default {
  // namespaced: true,

  state: {
    sportRegion_array: "",
    sportType_array: ""
  },
  getters: {
    get_sportRegion_array(state) {
      return state.sportRegion_array;
    },
    get_sportType_array(state) {
      return state.sportType_array;
    }
  },
  actions: {
    set_sportRegion_array({ state, dispatch, commit }) {
      // 常规 获取 ，有数值 就不再执行
      if (!state.sportRegion_array) {
        return dispatch("set_sportRegion_array_force");
      }
    },
    set_sportRegion_array_force({ commit }) {
      //强制获取， 强制获取最新数据 进行更新
      api_common.get_sportRegion_page({}).then(res => {
        let data = lodash.get(res, "data.data");
        console.log("数据提取");
        console.log(data);
        if (data) {
          data.unshift({
            id: "",
            introduction: "全部"
          });
        //   console.log(JSON.stringify(data));

          commit("set_sportRegion_array", data);
        }
      });
    },
    set_sportType_array({ state, dispatch, commit }) {
      // 常规 获取 ，有数值 就不再执行
      if (!state.sportType_array) {
        return dispatch("set_sportType_array_force");
      }
    },
    set_sportType_array_force({ commit }) {
      //强制获取， 强制获取最新数据 进行更新
      api_common.get_sportType_page({}).then(res => {
        let data = lodash.get(res, "data.data.records");
        console.log("数据提取");
        console.log(data);
        if (data) {
          // 需要前置处理
          commit("set_sportType_array", data);
        }
      });
    }
  },
  mutations: {
    set_sportRegion_array(state, sportRegion_array) {
      state.sportRegion_array = sportRegion_array;
    },
    set_sportType_array(state, sportType_array) {
      state.sportType_array = sportType_array;
    }
  }
};
