// import something here
import lodash from 'lodash';

// leave the export, even if you don't use it
export default async  ({ app, router, store, Vue }) => {
    // something to do
    Vue.prototype.$lodash = lodash;
    // router.beforeEach((to, from, next) => {
    //   //现在您需要在这里添加验证逻辑，比如调用一个API
    // })
}