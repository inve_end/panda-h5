// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js

module.exports = function(ctx) {
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"

    boot: ["i18n", "axios", "lodash"],
    //	preFetch	Boolean	Enable PreFetch Feature. 预加载 字辈路由  孙子辈的 不加载
    // preFetch: true,
    //css	Array	Global CSS/Stylus/… files from /src/css/, except for theme files, which are included by default.
    css: [
      /**
 *     'app.styl', // referring to /src/css/app.styl
     '~some-library/style.css' // referring to node_modules/some-library/style.css
 */
      "app.styl",
      "quasar_extend.styl",
      "common.styl",
      "panda.styl",
      // "panda_table.styl",
      "panda_color.styl",
      "panda_button.styl",

      // "scrollbar.styl",
      "pandaicon/variables.styl",
      "pandaicon/style.styl"
    ],
    //extras	Array	What to import from @quasar/extras package. Example: [‘material-icons’, ‘roboto-font’, ‘ionicons-v4’]
    extras: [
      "roboto-font",
      "material-icons" // optional, you are not bound to it
      // 'ionicons-v4',
      // 'mdi-v3',
      // 'fontawesome-v5',
      // 'eva-icons'
    ],

    //supportIE	Boolean	Add support for IE11+.
    supportIE: true,
    // htmlVariables	Object	Add variables that you can use in index.template.html.
    // htmlVariables: {},
    //animations	Object/String	What CSS animations to import. Example: [‘bounceInLeft’, ‘bounceOutRight’]
    animations: [],
    //framework	Object/String	What Quasar components/directives/plugins to import,
    // what Quasar language pack to use, what Quasar icon set to use for Quasar components.
    framework: {
      all: true, // --- includes everything; for dev only!

      // components: [
      //   'QLayout',
      //   'QHeader',
      //   'QDrawer',
      //   'QPageContainer',
      //   'QPage',
      //   'QToolbar',
      //   'QToolbarTitle',
      //   'QBtn',
      //   'QIcon',
      //   'QList',
      //   'QItem',
      //   'QItemSection',
      //   'QItemLabel',
      //   'QPageSticky',
      //   'QItem',
      //   'QCollapsible',

      // ],

      // directives: [
      //   'Ripple'
      // ],

      // plugins: ['Notify' /* ... */],

      // // Quasar config
      // // You'll see this mentioned for components/directives/plugins which use it

      config: {
        //https://quasar.dev/quasar-plugins/loading-bar
        loadingBar: {
          skipHijack: true
        }
      }

      // iconSet: 'material-icons', // default material-icons  requires icon library to be specified in "extras" section too,
      // https://github.com/quasarframework/quasar/tree/dev/ui/lang
      // lang: 'zh-hans', // Tell Quasar which language pack to use for its own components

      // cssAddon: true // Adds the flex responsive++ CSS classes (noticeable bump in footprint)
    },
    // https://quasar.dev/options/animations
    //https://daneden.github.io/animate.css/
    // animations: 'all', // --- includes all animations
    animations: [],
    // <!-- src/index.template.html -->
    // <%= htmlWebpackPlugin.options.title %>
    //这里的 htmlWebpackPlugin.options 可以直接读取到  package.json 里面的 数值
    htmlVariables: {
      title: "PAH5"
      // icon:"statics/favicon-icon.png",
    },
    //Use this property to change the default names of some files of your website/app if you have to.
    // All paths must be relative to the root folder of your project.
    // sourceFiles: {
    //   rootComponent: 'src/App.vue',
    //   router: 'src/router',
    //   store: 'src/store',
    //   indexHtmlTemplate: 'src/index.template.html',
    //   registerServiceWorker: 'src-pwa/register-service-worker.js',
    //   serviceWorker: 'src-pwa/custom-service-worker.js',
    //   electronMainDev: 'src-electron/main-process/electron-main.dev.js',
    //   electronMainProd: 'src-electron/main-process/electron-main.js'
    // },
    build: {
      // publicPath: '',
      //scopeHoisting: true,//	Boolean	Default: true. Use Webpack scope hoisting for slightly better runtime performance.
      // vueCompiler: true, //Include vue runtime + compiler version, instead of default Vue runtime-only
      //gzip: true,//	Boolean	Gzip the distributables. Useful when the web server with which you are serving the content does not have gzip.

      // analyze: true, //Show analysis of build bundle with webpack-bundle-analyzer. If using as Object, it represents the webpack-bundle-analyzer config Object.
      // extractCSS: false,
      /**
       * env: Add properties to process.env that you can use in your website/app JS code.
       *  Each property needs to be JSON encoded. Example: { SOMETHING: JSON.stringify(‘someValue’) }.
       */
      env: ctx.dev
        ? {
            // 在开发状态下我们拥有以下属性  process.env.API_PREFIX
            API_PREFIX_USER: JSON.stringify("api_user"),
            API_PREFIX_JOB: JSON.stringify("api_job"),
            API_PREFIX_FILE: JSON.stringify("file")
          }
        : {
            // 在构建状态（生产版本）下   process.env.API_PREFIX
            API_PREFIX_USER: JSON.stringify("pandah5user"),
            API_PREFIX_JOB: JSON.stringify("pandah5job"),
            API_PREFIX_FILE: JSON.stringify("file"),

            API_PREFIX_FILE: JSON.stringify("http://161.117.194.247:7788")
          },
      //https://quasar.dev/quasar-cli/cli-documentation/handling-webpack
      extendWebpack(cfg) {},
      // afterBuild() {
      // 打包后 可以直接 通过插件上传 Git ftp 等
      // },
      // vueRouterMode:'hash', // 默认 hash  history 模式需要服务端配合
      // htmlFilename:'index.html',	//String	Default is ‘index.html’.
      //productName	:'',//String	Default value is taken from package.json > productName field.
      // distDir:'',//Default is ‘dist/{ctx.modeName}’ // Applies to all Modes except for Cordova (which is forced to src-cordova/www).
      //https://webpack.js.org/configuration/devtool/
      //  devtool:'',//	String	Source map strategy to use.
      //https://github.com/webpack-contrib/terser-webpack-plugin/#minify
      uglifyOptions: {
        compress: {
          warnings: false,
          drop_console: true, //console
          pure_funcs: ["console.log"] //移除console
        }
      }
      // stylusLoaderOptions	Object	Options to supply to stylus-loader.
      // scssLoaderOptions	Object	Options to supply to sass-loader for .scss files.
      // sassLoaderOptions	Object	Options to supply to sass-loader for .sass files.
      // lessLoaderOptions	Object	Options to supply to less-loader.

      //preloadChunks:"true",//	Boolean	Default is “true”. Preload chunks when browser is idle to improve user’s later navigation to the other pages.

      // 以下 配置 默认，不要更改
      /**
       * The following properties of build are automatically configured by Quasar CLI depending on dev/build
       * commands and Quasar mode. But if you like to override some (make sure you know what you are doing), you can do so:
       */
      // extractCSS	Boolean	Extract CSS from Vue files
      // sourceMap	Boolean	Use source maps
      // minify	Boolean	Minify code (html, js, css)
      // webpackManifest	Boolean	Improves caching strategy. Use a webpack manifest (runtime) file to avoid cache bust on vendor chunk changing hash on each build.
    },
    /**
     * Webpack dev server options. Some properties are overwritten based on the Quasar mode you’re using in order to ensure a correct config.
     *  Note: if you’re proxying the development server (i.e. using a cloud IDE), set the public setting to your public application URL.
     */
    devServer: {
      // https://webpack.js.org/configuration/dev-server/

      // https: true,
      /**
       * When you set devServer > https: true in your quasar.conf.js file,
       * Quasar will auto-generate a SSL certificate for you. However,
       *  if you want to create one yourself for your localhost, then check out this blog post by Filippo.
       * Then your quasar.conf.js > devServer > https should look like this:
       */
      // const fs = require('fs')
      // https: {
      //   key: fs.readFileSync('/path/to/server.key'),
      //   cert: fs.readFileSync('/path/to/server.crt'),
      //   ca: fs.readFileSync('/path/to/ca.pem'),
      // }
      // port: 8080,
      // public:'',//	String	Public address of the application (for use with reverse proxies)
      //host:"",	String	Local IP/Host to use for dev server
      open: true, // opens browser window automatically
      port: 8080,
      proxy: {
        // 将所有以/api开头的请求代理到jsonplaceholder
        "/api_user": {
          // target: "http://127.0.0.1",
          // target: "http://47.75.137.92:6688", //  本地测试服务器的
          // target: "http://172.16.6.26:8068",  // sklee 本地
          // target: "http://172.16.6.110:8067", //   gardening   http://172.16.6.110:8067/swagger-ui.html#
          // target: "http://172.16.6.110:8068", // gardening  rap

          // target: "http://47.244.224.200:8067", //   业务测试地址   后台开发人员聊条地址 联调环境
          target: "http://161.117.195.63:8067", //   测试人员测试地址    上线用   测试环境

          // H5页面访问地址：http://161.117.194.247:81/   目录：/usr/local/nginx3/html_h5

          // target: "http://uuuu.uu",
          changeOrigin: true,
          pathRewrite: {
            "^/api_user": ""
          }
        },
        "/api_job": {
          // target: "http://127.0.0.1",
          // target: "http://47.75.137.92:6688", //  本地测试服务器的
          // target: "http://172.16.6.26:8068",  // sklee 本地
          // target: "http://172.16.6.110:8067", //   gardening   http://172.16.6.110:8067/swagger-ui.html#
          // target: "http://172.16.6.110:8068", // gardening  rap

          // target: "http://47.244.224.200:8068", //   业务测试地址   后台开发人员聊条地址  联调环境
          target: "http://161.117.195.63:8068", //   测试人员测试地址    上线用        测试环境

          // H5页面访问地址：http://161.117.194.247:81/   目录：/usr/local/nginx3/html_h5

          // target: "http://uuuu.uu",
          changeOrigin: true,
          pathRewrite: {
            "^/api_job": ""
          }
        },

        "/file": {
          // target: "http://127.0.0.1",
          // target: "http://47.244.249.191:7788", //http://47.244.249.191:9988/ 图片访问 路径前缀
          target: "http://161.117.194.247:7788", //http://161.117.194.247:7788/ 图片访问 路径前缀
          // target: "http://uuuu.uu",
          changeOrigin: true,
          pathRewrite: {
            "^/file": ""
          }
        }
      }
    },
    ssr: {
      pwa: false
    },
    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {}, // only for NON InjectManifest
      manifest: {
        // name: 'Quasar App',
        // short_name: 'Quasar-PWA',
        // description: 'Best PWA App in town!',
        display: "standalone",
        orientation: "portrait",
        background_color: "#ffffff",
        theme_color: "#027be3",
        icons: [
          {
            src: "statics/common/icons/icon-128_128.png",
            sizes: "128x128",
            type: "image/png"
          },
          {
            src: "statics/common/icons/icon-192_192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "statics/common/icons/icon-256_256.png",
            sizes: "256x256",
            type: "image/png"
          },
          {
            src: "statics/common/icons/icon-384_384.png",
            sizes: "384x384",
            type: "image/png"
          },
          {
            src: "statics/common/icons/icon-512_512.png",
            sizes: "512x512",
            type: "image/png"
          }
        ]
      }
    },

    cordova: {
      // id: 'org.cordova.quasar.app'
      // noIosLegacyBuildFlag: true // uncomment only if you know what you are doing
    },

    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack(cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Window only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration
        // appId: 'quasar-app'
      }
    }
  };
};
